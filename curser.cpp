#include "curser.hpp"

curser::curser()
{
	initscr();
	cbreak();
	curs_set(0);
	keypad(stdscr, true);
	noecho();
	nodelay(stdscr, true);
	start_color();
	init_pair(1, 1, 0);
	init_pair(2, 2, 0);
	init_pair(3, 3, 0);
	init_pair(4, 4, 0);
}

curser::~curser()
{
	endwin();
}
