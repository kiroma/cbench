#include <thread>
#include <chrono>
#include <iostream>
#include <fstream>
#include <functional>
#include <vector>
#include <atomic>
#include <string>
#include <algorithm>
#include <mutex>
#include "curser.hpp"

int FPSlimit = 60;
unsigned long long cycles = -1;

void blank()
{
	std::vector<int> a;
	a.push_back(1);
	a.pop_back();
}

typedef std::chrono::duration<unsigned int, std::nano> dur_t;

void benchmarker(std::function<void()> func, std::vector<dur_t> &durs, std::atomic<bool> &go, std::mutex &mtx)
{
	decltype(std::chrono::high_resolution_clock::now()) start, end;
	for(unsigned long long i = 0; i < cycles && go; ++i)
	{
		start = std::chrono::high_resolution_clock::now();
		func();
		end = std::chrono::high_resolution_clock::now();
		mtx.lock();
		durs.push_back(end-start);
		mtx.unlock();
	}
}

std::string getHelp()
{
	return "Usage: cbench [--help][--fps n][--cycles n][--nostats]\n[--help] - Display this dialogue and exit\n[--fps n] - Run at the rate of n cycles per second\n[--cycles n] - Run for n cycles and exit\n[--nostats] - Don't display the live statistics, only average is visible";
}

int main(int argc, char** argv)
{
	bool stats = true;
	for(unsigned short i = 1; i < argc; ++i)
	{
		if(std::string(argv[i]) == "--help")
		{
			std::cout << getHelp() << std::endl;
			return 0;
		}
		else if(std::string(argv[i]) == "--fps")
		{
			if(i+1 >= argc)
			{
				std::cerr << "Mismatched arguments!\n" << getHelp() << std::endl;
				return 0;
			}
			FPSlimit = std::stoi(argv[++i]);
		}
		else if(std::string(argv[i]) == "--cycles")
		{
			if(i+1 >= argc)
			{
				std::cerr << "Mismatched arguments!\n" << getHelp() << std::endl;
				return 0;
			}
			cycles = std::stoll(argv[++i]);
		}
		else if(std::string(argv[i]) == "--nostats")
		{
			stats = false;
		}
	}
	decltype(std::chrono::high_resolution_clock::now()) a;
	bool end = false;
	char input;
	double p1, p10, p50, p75, p97;
	std::mutex mtx;
	std::vector<dur_t> durs;
	std::atomic<bool> go {true};
	std::thread t(benchmarker, blank, std::ref(durs), std::ref(go), std::ref(mtx));
	{
		curser m_curser;
		while(!end)
		{
			a = std::chrono::high_resolution_clock::now();
			input = getch();
			switch(input)
			{
				case err:
					break;
				case 'q':
					end = true;
					break;
				default:
					printw("\nUnknown key %c", input);
					break;
			}
			std::vector<dur_t> copy;
			mtx.lock();
			if(stats)
			{
				std::sort(durs.begin(), durs.end());
			}
			copy = durs;
			mtx.unlock();
			size_t i = 0;
			dur_t totalruntime(0);
			for(;i < copy.size()-1; ++i)
			{
				totalruntime += copy[i];
			}
			if(i == cycles-1)
			{
				end = true;
			}
			double avg = totalruntime.count()/i;
			if(stats)
			{
				p1 = copy[copy.size()*0.01].count();
				p10 = copy[copy.size()*0.1].count();
				p50 = copy[copy.size()*0.5].count();
				p75 = copy[copy.size()*0.75].count();
				p97 = copy[copy.size()*0.97].count();
			}
			mvprintw(0, 0, "frame: %d\naverage time for each function call: %f\n", i, avg);
			if(stats)
			{
				printw("3rd percentile: %f\nMedian: %f\n97th percentile: %f", p1, p50, p97);
			}
			std::this_thread::sleep_for((std::chrono::duration<double>(1)/FPSlimit)-(std::chrono::high_resolution_clock::now()-a));
		}
		go = false;
	}
	t.join();
	std::sort(durs.begin(), durs.end());
	std::string inp;
	double total = 0;
	for(auto a : durs)
	{
		total += a.count();
	}
	p1 = durs[durs.size()*0.01].count();
	p10 = durs[durs.size()*0.1].count();
	p50 = durs[durs.size()*0.5].count();
	p75 = durs[durs.size()*0.75].count();
	p97 = durs[durs.size()*0.97].count();
	std::cout << std::fixed;
	std::cout << "1st percentile: " << p1
	<< "\n10th percentile: " << p10
	<< "\nMedian: " << p50
	<< "\n75th percentile: " << p75
	<< "\n97th percentile: " << p97 << std::endl;
	std::cout << "Average time per call: " << total/durs.size() << std::endl;
	std::cout << "Save data? y/n ";
	std::cin >> inp;
	if(inp == "y" || inp == "Y")
	{
		std::cout << "Specify output file: ";
		std::cin >> inp;
		std::ofstream ofs(inp, std::ios::binary);
		ofs << std::fixed;
		double last = 0;
		int count = 0;
		ofs << "1st percentile: " << p1
                << "\n10th percentile: " << p10
                << "\nMedian: " << p50
                << "\n75th percentile: " << p75
                << "\n97th percentile: " << p97
                << "\nAverage time per call: " << total/durs.size() << std::endl;
		for(auto a : durs)
		{
			if(a.count() != last)
			{
				if(count != 0)
				{
					ofs << " " << count << '\n';
				}
				last = a.count();
				ofs << a.count();
				count = 0;
			}
			++count;
		}
		ofs << " " << count << '\n';
	}
}
